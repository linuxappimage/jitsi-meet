# jitsi-meet

jitsi-meet AppImage version

## jitsi-meet-electron

[Jitsi Meet desktop application](https://github.com/jitsi/jitsi-meet-electron)

Download **jitsi-meet-electron AppImage** binary from here: [jitsi-meet-x86_64.AppImage](https://gitlab.com/linuxappimage/jitsi-meet/-/jobs/artifacts/master/raw/jitsi-meet-x86_64.AppImage?job=run-build)

Notes for you Linux shell:

```sh
curl -sLo jitsi-meet-x86_64.AppImage \
  https://gitlab.com/linuxappimage/jitsi-meet/-/jobs/artifacts/master/raw/jitsi-meet-x86_64.AppImage?job=run-build

chmod +x jitsi-meet-x86_64.AppImage

./jitsi-meet-x86_64.AppImage

```

## Linux Deps

you need to install nis v3 library

eg. in archlinux

```bash
sudo pacman -S extra/libnss_nis
```

### Optional Deps

one of these:

* zenity
* kdialog
* Xdialog

eg.

```bash
sudo pacman -S community/xdialog
```

## Possible Error

If you get this error:

```bash
$ ./jitsi-meet-x86_64.AppImage
A JavaScript error occurred in the main process
Uncaught Exception:
Error: /tmp/.org.chromium.Chromium.GZuzb9: failed to map segment from shared object
```

You need to make your /tmp executable

```bash
sudo mount -o rw,exec /tmp/
```

alternative, you need to change your TMPDIR environment variable

eg. if your home dir has exec mount perms:

```bash
mkdir -p ~/.tmpdir/ && env TMPDIR=~/.tmpdir/ /opt/AppImage/Jitsi-meet-x86_64.AppImage

```
